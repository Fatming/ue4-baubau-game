// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MyBaseCharacter.generated.h"

UCLASS()
class CHARACTERANIMATION_API AMyBaseCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	//
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MyBaseCharacter")
		float m_fHealth = 100;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "MyBaseCharacter")
		bool m_bIsDead = false;
	//
	virtual	void	CalculateDead();
	UFUNCTION(BlueprintCallable,Category = "MyBaseCharacter")
		virtual	void	CalculateHealth(float e_fDamage);
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& e_FPropertyChangeEvent) override;
#endif

public:
	// Sets default values for this character's properties
	AMyBaseCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};
