// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterAnimation.h"
#include "MyBaseCharacter.h"


// Sets default values
AMyBaseCharacter::AMyBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyBaseCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AMyBaseCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}


//
void	AMyBaseCharacter::CalculateDead()
{
	if (m_fHealth <= 0)
		this->m_bIsDead = true;
	else
		this->m_bIsDead = false;
}

void	AMyBaseCharacter::CalculateHealth(float e_fDamage)
{
	m_fHealth -= e_fDamage;
	CalculateDead();
}
#if WITH_EDITOR
void AMyBaseCharacter::PostEditChangeProperty(FPropertyChangedEvent& e_FPropertyChangeEvent)
{
	m_fHealth = 100;
	this->m_bIsDead = false;
	Super::PostEditChangeProperty(e_FPropertyChangeEvent);
	this->CalculateDead();
}
#endif