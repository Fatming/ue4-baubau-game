// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PickUp.h"
#include "BatteryPickUp.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERANIMATION_API ABatteryPickUp : public APickUp
{
	GENERATED_BODY()
public:
	ABatteryPickUp();
	
	
};
