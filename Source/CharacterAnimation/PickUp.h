// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PickUp.generated.h"

UCLASS()
class CHARACTERANIMATION_API APickUp : public AActor
{
	GENERATED_BODY()
private:
	/** StaticMeshComponent to represent the pickup in the level. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pickup,meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* PickupMesh;
protected:
	///** True when the pickup is able to be picked up, false if something deactivates the pickup. */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)
	bool bIsActive;
public:	
	// Sets default values for this actor's properties
	APickUp();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//
	FORCEINLINE UStaticMeshComponent* GetPickupMesh() { return PickupMesh; }
	UFUNCTION(BlueprintPure, Category = "PickUp")
	bool		IsActive(){return this->bIsActive;}
	UFUNCTION(BlueprintCallable, Category = "PickUp")
	void		SetActive(bool e_bActive) { this->bIsActive = e_bActive; }
	
};
